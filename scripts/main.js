// 1)
let enterNumber;

do {
    enterNumber = +prompt("Введите число");
} while (!Number.isInteger(enterNumber))


if (enterNumber < 5) {
    console.log(`Sorry, no numbers`);
} else {
    for (let i = 0;  i <= enterNumber; i++) {    
        if (i % 5 === 0) {
            console.log(i);
        }
    }
}


// 2) простые  числа

let m;
let n;

do {
    m = +prompt("Введите число");
    n = +prompt("Введите число");
} while (!Number.isInteger(m) || !Number.isInteger(n))


for (let i = m; i <= n; i++) {
    for (let j = m; j <= i; j++) {
      if (i % j === 0 && j < i) {
        break;
      } else if (j === i) {
        console.log(i);
      }
    }
}

